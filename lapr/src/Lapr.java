
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.NamedPlotColor;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.FileTerminal;
import com.panayotis.gnuplot.terminal.GNUPlotTerminal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import static java.lang.Math.abs;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Lapr {

    static Scanner ler = new Scanner(System.in, "ISO-8859-1");
    final static int FILEINFO_SECONDS = 0; //Index dos segundos no array fileInfo
    final static int FILEINFO_MINUTES = 1; //Index dos minutos no array fileInfo
    final static int FILEINFO_HOURS = 2; //Index das horas no array fileInfo
    final static int FILEINFO_DAYS = 3; //Index dos dias no array fileInfo
    final static int FILEINFO_MONTHS = 4; //Index dos meses no array fileInfo
    final static int FILEINFO_YEARS = 5; //Index dos anos no array fileInfo
    final static int FILEINFO_WATTS = 6; //Index do consumo de eletricidade no array fileInfo
    final static int MAXDATA = 400000; //Quantidade maxima de linhas num array
    final static int INFORMATIONS = 7; //Quantidade maxima de colunas num array
    final static int OUTPUTFILE_ORIGIN = 0;
    final static int OUTPUTFILE_FILTER = 1;
    final static int OUTFILE_DATE = 2;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String[][] fileInfo = new String[MAXDATA][INFORMATIONS]; //O array que guarda as informacoes do ficheiro recebido como parametro
        boolean endProgram = false, modoInterativo; //Usado para definir quando acabar o programa, e se os modulos funcionam em modo interativo ou não interativo
        String[] outputFileInfo = new String[3]; //Usado para definir o nome dos ficheiros de output 

        if (args.length == 2 && args[0].compareTo("-nome") == 0) { //Modo interativo, pede 2 argumentos
            modoInterativo = true;
            leitura(outputFileInfo, args[1], fileInfo, modoInterativo); //Lê e organiza os dados do ficheiro
            int qntObserv = numObservacoesTotal(fileInfo); //Guarda a quantidade de dados existentes
            int resolucao = analise(outputFileInfo, fileInfo, qntObserv, 0, true); // Definir a resolução 
            qntObserv = numObservacoesTotal(fileInfo); //Guarda a quantidade nova de dados existentes

            while (endProgram == false) {//Repetir até o programa ser fechado
                /*
                Imprimir instruções do menu
                 */
                System.out.println("0 - Fechar o programa.");
                System.out.println("1 - Calcular número de observações.");
                System.out.println("2 - Ordenar valores da série temporal.");
                System.out.println("3 - Calcular médias.");
                System.out.println("4 - Calcular erro médio absoluto.");
                System.out.println("5 - Prever uma observação.");
                System.out.println("6 - Carregar nova série temporal.");

                int selecao = ler.nextInt(); //Selecionar a opção no menu
                while (selecao < 0 || selecao > 8) { //confirmar se seleciona um numero/caracter válido
                    System.out.println("Seleção inválida, por favor tente novamente"); //Pedir nova seleção
                    selecao = ler.nextInt(); //Nova selecao
                }

                switch (selecao) {
                    case 0://Fechar o programa
                        endProgram = true;
                        break;
                    case 1://Calcular número de observações
                        numObservacoes(fileInfo, outputFileInfo, qntObserv, true);
                        break;
                    case 2://Ordenar valores de forma crescente/decrescente
                        listagem(outputFileInfo, fileInfo, qntObserv, true, 0);
                        break;
                    case 3://Calcular médias
                        media(outputFileInfo, fileInfo, qntObserv, true, 0, 0);
                        break;
                    case 4://Calcular erro médio absoluto
                        erroMedioAbsoluto(fileInfo, outputFileInfo, qntObserv, false, 0, 0);
                        break;
                    case 5://Prever uma observação
                        prever(outputFileInfo, fileInfo, qntObserv, resolucao, 0, modoInterativo, 0, "");
                        break;
                    case 6://Carregar nova série
                        carregarNovoFicheiro(outputFileInfo, fileInfo);
                        qntObserv = numObservacoesTotal(fileInfo); //Guarda a nova quantidade de observações
                        resolucao = analise(outputFileInfo, fileInfo, qntObserv, 0, true); // Definir a resolução, devolver novo valor total de observações
                        qntObserv = numObservacoesTotal(fileInfo); //Guarda a nova quantidade de observações
                        break;
                }
            }
        } else if (args.length == 12) { //Modo não interativo, pede 12 argumentos
            int X = Integer.parseInt(args[3]); //Periodo/Diario/Mensal/anual
            int M = Integer.parseInt(args[5]); //Tipo de média
            int T = Integer.parseInt(args[7]); //Ordem crescente/decrescente
            float nAlfa = Float.parseFloat(args[9]); //n ou alfa
            String D = args[11]; //Data a ser prevista
            modoInterativo = false; //Definir o modo como não interativo
            verificarArgumentos(X, M, T, nAlfa, D);

            leitura(outputFileInfo, args[1], fileInfo, modoInterativo); //Lê e organiza os dados do ficheiro
            int qntObserv = numObservacoesTotal(fileInfo); //Guarda a quantidade de dados existentes
            analise(outputFileInfo, fileInfo, qntObserv, X, modoInterativo); // Definir a resolução 
            qntObserv = numObservacoesTotal(fileInfo); //Guarda a quantidade nova de dados existentes
            numObservacoes(fileInfo, outputFileInfo, qntObserv, modoInterativo); //Guardar número de observações em intervalos
            listagem(outputFileInfo, fileInfo, qntObserv, modoInterativo, T); //Listar crescente/decrescente
            media(outputFileInfo, fileInfo, qntObserv, modoInterativo, M, nAlfa); //Fazer a Média
            erroMedioAbsoluto(fileInfo, outputFileInfo, qntObserv, modoInterativo, M, nAlfa); //Calcula o erro medio absoluto
            prever(outputFileInfo, fileInfo, qntObserv, X, nAlfa, modoInterativo, M, D); //Prever um valor de consumo

        } else {
            System.out.println("Numero de argumentos incorreto ou argumentos invalidos.");
        }
    }

    public static void verificarArgumentos(int X, int M, int T, float nAlfa, String D) {
        boolean validArguments = true;

        if (X != 11 && X != 12 && X != 13 && X != 14 && X != 2 && X != 3 && X != 4) {
            System.out.println("Resolução inválida");
            validArguments = false;
        }

        if (M != 1 && M != 2) {
            System.out.println("Escolha de modelo de média inválida");
            validArguments = false;
        }

        if (T != 1 && T != 2) {
            System.out.println("Escolha de modelo de ordenação inválida");
            validArguments = false;
        }

        if ((M == 1 && nAlfa < 0) || (M == 2 && (nAlfa >= 1 || nAlfa <= 0))) {
            System.out.println("Valor de nAlfa inválido");
            validArguments = false;
        }

        if ((X == 4 && D.split("-").length != 1) || (X == 3 && D.split("-").length != 2) || (X == 2 && D.split("-").length != 3) || ((X == 11 || X == 12 || X == 13 || X == 14) && D.split("-").length != 4)) {
            System.out.println("Data inválida");
            validArguments = false;
        }

        if (validArguments == false) {
            System.exit(0);
        }
    }

    public static void leitura(String[] outputFileInfo, String fileName, String[][] fileInfo, boolean modoInterativo) throws FileNotFoundException {

        while (new File(fileName).isFile() == false) { //confirmar se o ficheiro recebido e valido
            if (modoInterativo == true) {
                System.out.println("Ficheiro invalido ou inexistente, por favor inserir novo ficheiro:"); //Pede um ficheiro novo
                fileName = ler.nextLine(); //Ler um ficheiro novo
            } else {
                System.out.println("Ficheiro invalido ou inexistente."); //Avisa que o ficheiro não existe
                System.exit(0); //Fecha o programa
            }
        }

        String[] outputFileName = fileName.split("\\."); //Separar o nome do ficheiro da extensão (i.e. DAYTON.csv >> DAYTON | csv)
        outputFileInfo[OUTPUTFILE_ORIGIN] = outputFileName[0]; //Guardar nome do ficheiro original, sem extensão

        storeFileInfo(fileName, fileInfo); //Guarda a quantidade de linhas com dados, e insere os mesmos em estruturas
    }

    public static void storeFileInfo(String fileName, String[][] fileInfo) throws FileNotFoundException {
        Scanner csv = new Scanner(new File(fileName));  //armazenar os dados do ficheiro passado por parametro 
        /*
        * Constantes para definir o index a usar para leitura do ficheiro
         */
        final int FILE_DAYS = 2;
        final int FILE_MONTHS = 1;
        final int FILE_YEARS = 0;
        final int FILE_SECONDS = 2;
        final int FILE_MINUTES = 1;
        final int FILE_HOURS = 0;
        final int FILE_WATTS = 1;

        int i = 0; //Iterador

        csv.nextLine(); //Ignorar o cabeçalho
        while (csv.hasNextLine()) {
            String linha = csv.nextLine(); //Ler a nova linha
            String[] lineInfoRaw = linha.split(","); //Divide a informacao de data da informacao do consumo
            fileInfo[i][FILEINFO_WATTS] = lineInfoRaw[FILE_WATTS]; //Guarda a informacao do consumo

            String[] divideDate = lineInfoRaw[0].split(" "); //Divide os Dias/Meses/Anos dos Segundos/Minutos/Horas
            String[] dateYearsMonthsDays = divideDate[0].split("-"); //Divide os Dias,Meses eAnos
            fileInfo[i][FILEINFO_DAYS] = dateYearsMonthsDays[FILE_DAYS]; //Guarda os Dias
            fileInfo[i][FILEINFO_MONTHS] = dateYearsMonthsDays[FILE_MONTHS]; //Guarda os Meses
            fileInfo[i][FILEINFO_YEARS] = dateYearsMonthsDays[FILE_YEARS]; //Guarda os Anos
            String[] dateHoursMinutesSeconds = divideDate[1].split(":"); //Divide os Segunds, Minutos e Horas
            fileInfo[i][FILEINFO_SECONDS] = dateHoursMinutesSeconds[FILE_SECONDS]; //Guarda os Segundos
            fileInfo[i][FILEINFO_MINUTES] = dateHoursMinutesSeconds[FILE_MINUTES]; //Guarda os Minutos
            fileInfo[i][FILEINFO_HOURS] = dateHoursMinutesSeconds[FILE_HOURS]; //Guarda as Horas

            i++; //Incrementa para ler a linha seguinte
        }
        csv.close(); //Fecha o ficheiro
    }

    public static void carregarNovoFicheiro(String[] outputFileInfo, String[][] fileInfo) throws FileNotFoundException {
        String newFileName; //Variavel que guarda o endereço do novo ficheiro

        System.out.println("Inserir endereço de novo ficheiro:"); //Pedir novo ficheiro
        ler.nextLine();
        newFileName = ler.nextLine(); //Ler um ficheiro novo  

        leitura(outputFileInfo, newFileName, fileInfo, true); //Lê e organiza os dados do ficheiro, guarda a quantidade de dados existentes

        confirmarContinuar(); //Confirmar e voltar ao menu
    }

    public static int numObservacoesTotal(String[][] fileInfo) {
        int i = 0; //Iterador
        while (fileInfo[i][FILEINFO_WATTS] != null) {//Contar numero de observações total
            i++;
        }
        return (i);//Devolver número de observações total
    }

    public static int analise(String[] outputFileInfo, String[][] fileInfo, int qtData, int resolucao, boolean modoInterativo) throws IOException {
        int[] wattsValues = new int[MAXDATA]; //Guarda os valores dos Watts

        if (modoInterativo == true) {//Caso seja interativo, pedir a resolução
            /*Menu de escolha da resolução*/
            System.out.println("Indique a resolução temporal:");
            System.out.println("1 - Periodico");
            System.out.println("2 - Diário");
            System.out.println("3 - Mensal");
            System.out.println("4 - Anual");

            resolucao = ler.nextInt();
            while (resolucao < 1 || resolucao > 4) {
                System.out.println("Resolução inválida, por favor tente outra vez.");
                resolucao = ler.nextInt();
            }
        }

        switch (resolucao) {
            //Anual
            case 4:
                calcularAnalise(fileInfo, wattsValues, qtData, FILEINFO_YEARS);
                outputFileInfo[1] = "analiseAnual";
                break;
            //Mensal
            case 3:
                calcularAnalise(fileInfo, wattsValues, qtData, FILEINFO_MONTHS);
                outputFileInfo[1] = "analiseMensal";
                break;
            //Diária
            case 2:
                calcularAnalise(fileInfo, wattsValues, qtData, FILEINFO_DAYS);
                outputFileInfo[1] = "analiseDiaria";
                break;
            //Periodica
            case 1:
            case 11:
            case 12:
            case 13:
            case 14:
                calcularAnalise(fileInfo, wattsValues, qtData, FILEINFO_HOURS);
                outputFileInfo[1] = "analisePeriodica";
                break;
            default:
                //Caso a escolha seja inválida, fechar o programa (apenas acontece em modo não interativo);
                System.out.println("Resolução inválida.");
                System.exit(0);
                break;
        }
        return (resolucao); //Devolver a resolucao
    }

    public static void calcularAnalise(String[][] fileInfo, int[] wattsValues, int qntObserv, int fileInfoIndex) {
        int k = 0, i = 0, currentDateIterator = 0; //Iteradores

        if (fileInfoIndex == 2) { //Analise Periodica
            for (i = 1; i < qntObserv; i++) {
                if (fileInfo[i][fileInfoIndex].compareTo("00") != 0 && fileInfo[i][fileInfoIndex].compareTo("06") != 0 && fileInfo[i][fileInfoIndex].compareTo("12") != 0 && fileInfo[i][fileInfoIndex].compareTo("18") != 0) {
                    wattsValues[k] += Integer.parseInt(fileInfo[i][FILEINFO_WATTS]);
                } else {
                    fileInfo[k][FILEINFO_SECONDS] = fileInfo[i - 1][FILEINFO_SECONDS];
                    fileInfo[k][FILEINFO_MINUTES] = fileInfo[i - 1][FILEINFO_MINUTES];
                    fileInfo[k][FILEINFO_HOURS] = fileInfo[i - 1][FILEINFO_HOURS];
                    fileInfo[k][FILEINFO_DAYS] = fileInfo[i - 1][FILEINFO_DAYS];
                    fileInfo[k][FILEINFO_MONTHS] = fileInfo[i - 1][FILEINFO_MONTHS];
                    fileInfo[k][FILEINFO_YEARS] = fileInfo[i - 1][FILEINFO_YEARS];
                    fileInfo[k][FILEINFO_WATTS] = Integer.toString(wattsValues[k]);
                    k++;
                    wattsValues[k] += Integer.parseInt(fileInfo[i][FILEINFO_WATTS]);
                }
            }
        } else {
            for (i = 0; i < qntObserv; i++) { //Analise diária, mensal ou anual
                if (fileInfo[i][fileInfoIndex].compareTo(fileInfo[currentDateIterator][fileInfoIndex]) == 0) {
                    wattsValues[k] += Integer.parseInt(fileInfo[i][FILEINFO_WATTS]);
                } else {
                    fileInfo[k][FILEINFO_SECONDS] = fileInfo[i - 1][FILEINFO_SECONDS];
                    fileInfo[k][FILEINFO_MINUTES] = fileInfo[i - 1][FILEINFO_MINUTES];
                    fileInfo[k][FILEINFO_HOURS] = fileInfo[i - 1][FILEINFO_HOURS];
                    fileInfo[k][FILEINFO_DAYS] = fileInfo[i - 1][FILEINFO_DAYS];
                    fileInfo[k][FILEINFO_MONTHS] = fileInfo[i - 1][FILEINFO_MONTHS];
                    fileInfo[k][FILEINFO_YEARS] = fileInfo[i - 1][FILEINFO_YEARS];
                    fileInfo[k][FILEINFO_WATTS] = Integer.toString(wattsValues[k]);
                    currentDateIterator = i;
                    k++;
                    wattsValues[k] += Integer.parseInt(fileInfo[i][FILEINFO_WATTS]);
                }
            }
        }
        /*Definir a última observação*/
        fileInfo[k][FILEINFO_SECONDS] = fileInfo[i][FILEINFO_SECONDS];
        fileInfo[k][FILEINFO_MINUTES] = fileInfo[i][FILEINFO_MINUTES];
        fileInfo[k][FILEINFO_HOURS] = fileInfo[i][FILEINFO_HOURS];
        fileInfo[k][FILEINFO_DAYS] = fileInfo[i][FILEINFO_DAYS];
        fileInfo[k][FILEINFO_MONTHS] = fileInfo[i][FILEINFO_MONTHS];
        fileInfo[k][FILEINFO_YEARS] = fileInfo[i][FILEINFO_YEARS];
        fileInfo[k][FILEINFO_WATTS] = Integer.toString(wattsValues[k]);

        for (int y = k + 1; y < qntObserv; y++) { //Limpar observações extra
            fileInfo[y][FILEINFO_SECONDS] = null;
            fileInfo[y][FILEINFO_MINUTES] = null;
            fileInfo[y][FILEINFO_HOURS] = null;
            fileInfo[y][FILEINFO_DAYS] = null;
            fileInfo[y][FILEINFO_MONTHS] = null;
            fileInfo[y][FILEINFO_YEARS] = null;
            fileInfo[y][FILEINFO_WATTS] = null;
        }
    }

    public static void numObservacoes(String[][] fileInfo, String[] outputFileInfo, int qntObserv, boolean modoInterativo) throws IOException {
        int firstInterval = 0, secondInterval = 0, thirdInterval = 0, mediaGlobal = 0;

        for (int i = 0; i < qntObserv; i++) {
            mediaGlobal += Integer.parseInt(fileInfo[i][FILEINFO_WATTS]);
        }
        mediaGlobal = mediaGlobal / qntObserv;

        for (int i = 0; i < qntObserv; i++) {
            if (Integer.parseInt(fileInfo[i][FILEINFO_WATTS]) <= (int) (mediaGlobal - 0.2 * mediaGlobal)) {
                firstInterval++;
            } else if (Integer.parseInt(fileInfo[i][FILEINFO_WATTS]) >= (int) (mediaGlobal + 0.2 * mediaGlobal)) {
                thirdInterval++;
            } else {
                secondInterval++;
            }
        }

        if (modoInterativo == true) { //Caso seja modo interativo, mostrar o output na consola
            System.out.println("Número de observações no primeiro intervalo: " + firstInterval);
            System.out.println("Número de observações no segundo intervalo: " + secondInterval);
            System.out.println("Número de observações no terceiro intervalo: " + thirdInterval);

            ler.nextLine();
            confirmarContinuar(); //Confirmar e voltar ao menu
        } else { //Caso seja modo não interativo, guardar informação num ficheiro .txt
            outputFileInfo[1] = "info";
            String fileName = getFileName(outputFileInfo, ".txt");

            FileWriter file = new FileWriter(fileName + ".txt");
            BufferedWriter buffWriter = new BufferedWriter(file);
            buffWriter.append("Número de observações no primeiro intervalo: " + firstInterval);
            buffWriter.newLine();
            buffWriter.append("Número de observações no segundo intervalo: " + secondInterval);
            buffWriter.newLine();
            buffWriter.append("Número de observações no terceiro intervalo: " + thirdInterval);
            buffWriter.newLine();
            buffWriter.close();
        }
    }

    public static void listagem(String[] outputFileInfo, String[][] fileInfo, int qntObserv, boolean modoInterativo, int selecaoLista) throws IOException {
        int[] wattsValues = new int[MAXDATA];

        for (int i = 0; i < qntObserv; i++) {
            wattsValues[i] = Integer.parseInt(fileInfo[i][FILEINFO_WATTS]); //Guarda apenas os watts num array do tipo int
        }

        if (modoInterativo == true) {
            /*
        Imprime instruções do menu, no modo interativo
             */
            System.out.println("1 - Organizar por ordem crescente.");
            System.out.println("2 - Organizar por ordem decrescente.");

            selecaoLista = ler.nextInt(); //Receber a seleção no menu
            while (selecaoLista != 1 && selecaoLista != 2) { //Verificar se a seleção é válida
                System.out.println("Valor inválido, digite novamente"); //Pedir nova seleção caso não seja válida
                selecaoLista = ler.nextInt(); //Ler nova seleção
            }
        }

        switch (selecaoLista) {
            case 1: //Caso selecione a opção 1, organizar de forma crescente
                mergeSortCrescente(wattsValues, 0, qntObserv);
                outputFileInfo[OUTPUTFILE_FILTER] = "crescente";
                break;
            case 2: //Caso selecione a opção 2, organizar de forma decrescente
                mergeSortDecrescente(wattsValues, 0, qntObserv);
                outputFileInfo[OUTPUTFILE_FILTER] = "decrescente";
                break;
        }

        gnuPlot(outputFileInfo, wattsValues, qntObserv, modoInterativo); //Gerar o gráfico, guardar os ficheiros
    }

    public static void crescenteMS(int[] arr, int min, int meio, int max) {

        int n1 = meio - min + 1;
        int n2 = max - meio;

        int[] esq = new int[n1];
        int[] dir = new int[n2];
        for (int i = 0; i < n1; i++) {
            esq[i] = arr[min + i];
        }
        for (int j = 0; j < n2; j++) {
            dir[j] = arr[meio + 1 + j];
        }

        int i, j, k;
        i = 0;
        j = 0;
        k = min;

        while (i < n1 && j < n2) {
            if (esq[i] <= dir[j]) {
                arr[k] = esq[i];
                i++;
            } else {
                arr[k] = dir[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = esq[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = dir[j];
            j++;
            k++;
        }

    }

    public static void mergeSortCrescente(int[] arr, int min, int max) {

        if (min < max) { //Verificar se o minimo é menor do que o máximo
            int m = min + (max - min) / 2;

            mergeSortCrescente(arr, min, m);
            mergeSortCrescente(arr, m + 1, max);

            crescenteMS(arr, min, m, max);
        }
    }

    public static void decrescenteMS(int[] arr, int min, int meio, int max) {

        int n1 = meio - min + 1;
        int n2 = max - meio;

        int[] esq = new int[n1];
        int[] dir = new int[n2];
        for (int i = 0; i < n1; i++) {
            esq[i] = arr[min + i];
        }
        for (int j = 0; j < n2; j++) {
            dir[j] = arr[meio + 1 + j];
        }

        int i, j, k;
        i = 0;
        j = 0;
        k = max;

        while (i < n1 && j < n2) {
            if (esq[i] < dir[j]) {
                arr[k] = esq[i];
                i++;
            } else {
                arr[k] = dir[j];
                j++;
            }
            k--;
        }

        while (i < n1) {
            arr[k] = esq[i];
            i++;
            k--;
        }

        while (j < n2) {
            arr[k] = dir[j];
            j++;
            k--;
        }
    }

    public static void mergeSortDecrescente(int arr[], int min, int max) {
        if (min < max) {
            int m = min + (max - min) / 2;

            mergeSortCrescente(arr, min, m);
            mergeSortCrescente(arr, m + 1, max);

            decrescenteMS(arr, min, m, max);
        }
    }

    public static void media(String[] outputFileInfo, String[][] fileInfo, int numObserv, boolean modoInterativo, int selecaoLista, float nAlfa) throws IOException {

        if (modoInterativo == true) {
            /*
            Imprime instruções do menu
             */
            System.out.println("1 - Média Móvel Simples.");
            System.out.println("2 - Média Móvel Exponencialmente Pesada.");

            selecaoLista = ler.nextInt(); //Receber a seleção no menu
            while (selecaoLista != 1 && selecaoLista != 2) { //Verificar se a seleção é válida
                System.out.println("Valor inválido, digite novamente"); //Pedir nova seleção caso não seja válida
                selecaoLista = ler.nextInt(); //Ler nova seleção
            }
        }

        switch (selecaoLista) {
            case 1: //Caso selecione a opção 1, organizar de forma crescente
                if (modoInterativo == true) {
                    nAlfa = definirOrdem(numObserv);
                }
                int[] fileInfoMMS = MMS(fileInfo, numObserv, (int) nAlfa);
                outputFileInfo[OUTPUTFILE_FILTER] = "MMS";
                gnuPlot(outputFileInfo, fileInfoMMS, numObserv - (int) nAlfa, modoInterativo);
                break;
            case 2: //Caso selecione a opção 2, organizar de forma decrescente
                if (modoInterativo == true) {
                    nAlfa = definirAlfa();
                }
                int[] fileInfoMMEP = MMEP(fileInfo, numObserv, nAlfa);
                outputFileInfo[OUTPUTFILE_FILTER] = "MMEP";
                gnuPlot(outputFileInfo, fileInfoMMEP, numObserv, modoInterativo);
                break;
        }
    }

    public static int[] MMS(String[][] fileInfoRaw, int numObserv, int n) throws IOException {
        int somaMMS; //Variavel usada para calculo da filtragem

        int[] fileInfoMMS = new int[numObserv - (n - 1)]; //Usado para guardar valores depois da filtragem

        for (int i = n - 1; i < numObserv; i++) {
            somaMMS = 0;
            for (int k = i - (n - 1); k <= i; k++) {
                somaMMS += Integer.parseInt(fileInfoRaw[k][FILEINFO_WATTS]);
            }
            fileInfoMMS[i - (n - 1)] = somaMMS / n;
        }

        return (fileInfoMMS);
    }

    public static int[] MMEP(String[][] fileInfo, int numObserv, float alfa) throws IOException {
        int[] fileInfoMMEP = new int[numObserv];

        fileInfoMMEP[0] = (int) (alfa * Integer.parseInt(fileInfo[0][FILEINFO_WATTS]) + (1 - alfa) * Integer.parseInt(fileInfo[0][FILEINFO_WATTS]));
        for (int i = 1; i < numObserv; i++) {
            fileInfoMMEP[i] = (int) (alfa * Integer.parseInt(fileInfo[i - 1][FILEINFO_WATTS]) + (1 - alfa) * fileInfoMMEP[i - 1]);
        }

        return (fileInfoMMEP);
    }

    public static float definirAlfa() {
        System.out.println("Defina a constante alfa:"); //Pedir constante alfa
        float alfa = ler.nextFloat(); //Receber constante alfa

        while (alfa <= 0 || alfa >= 1) { //Caso alfa seja menor do que 0 ou maior do que 1, pedir novamente
            System.out.println("Valor de alfa inválido, por favor tente novamente:");
            alfa = ler.nextFloat();
        }

        return (alfa);
    }

    public static int definirOrdem(int numObserv) {//Define a ordem a ser usada

        System.out.println("Defina a ordem da média móvel:");
        int n = ler.nextInt(); //Receber valor da ordem
        while (n >= numObserv || n < 1) {// Caso n seja inválido
            System.out.println("Valor da ordem inválida, defina a ordem da média móvel:");
            n = ler.nextInt();
        }

        return (n); //Devolver a ordem
    }

    public static void erroMedioAbsoluto(String[][] fileInfo, String[] outputFileInfo, int qntObserv, boolean modoInterativo, int selecao, float nAlfa) throws IOException {
        double erroMMS = 0, erroMMEP = 0;

        if (selecao == 1 || selecao == 0) {
            if (modoInterativo == true) {
                nAlfa = definirOrdem(qntObserv);
            }
            int[] fileInfoMMS = MMS(fileInfo, qntObserv, (int) nAlfa);
            for (int i = 0; i < qntObserv; i++) {
                if (i < qntObserv - nAlfa) {
                    erroMMS += abs(fileInfoMMS[i] - Integer.parseInt(fileInfo[i][FILEINFO_WATTS]));
                }
            }
        }

        if (selecao == 2 || selecao == 0) {
            if (modoInterativo == true) {
                nAlfa = definirAlfa();
            }
            int[] fileInfoMMEP = MMEP(fileInfo, qntObserv, nAlfa);
            for (int i = 0; i < qntObserv; i++) {
                erroMMEP += abs(fileInfoMMEP[i] - Integer.parseInt(fileInfo[i][FILEINFO_WATTS]));
            }
        }

        if (modoInterativo == true) {
            System.out.print("Erro Médio Absoluto para Média Móvel Simples: ");
            System.out.printf("%.2f", erroMMS / qntObserv);
            System.out.println();
            System.out.print("Erro Médio Absoluto para Média Móvel Exponencialmente Pesada: ");
            System.out.printf("%.2f", erroMMEP / qntObserv);
            System.out.println();

            ler.nextLine();
            confirmarContinuar(); //Confirmar e voltar ao menu
        } else {
            outputFileInfo[1] = "info";
            String fileName = (outputFileInfo[0] + "_" + outputFileInfo[1] + "_" + outputFileInfo[2]);;
            FileWriter file = new FileWriter(fileName + ".txt", true);
            BufferedWriter buffWriter = new BufferedWriter(file);

            if (selecao == 1) {
                buffWriter.write("Erro Médio Absoluto para Média Móvel Simples: " + erroMMS / qntObserv);
            } else {
                buffWriter.write("Erro Médio Absoluto para Média Móvel Simples: " + erroMMEP / qntObserv);
            }
            buffWriter.newLine();
            buffWriter.close();
        }
    }

    public static void prever(String[] outputFileInfo, String[][] fileInfo, int qntObserv, int resolucao, float nAlfa, boolean modoInterativo, int selecaoLista, String data) throws IOException {
        if (modoInterativo == true) {
            /*
            Imprime instruções do menu
             */
            System.out.println("1 - Prever com Média Móvel Simples.");
            System.out.println("2 - Prever com Média Móvel Exponencialmente Pesada.");

            selecaoLista = ler.nextInt(); //Receber a seleção no menu
            while (selecaoLista != 1 && selecaoLista != 2) { //Verificar se a seleção é válida
                System.out.println("Valor inválido, digite novamente"); //Pedir nova seleção caso não seja válida
                selecaoLista = ler.nextInt(); //Ler nova seleção
            }
        }
        switch (selecaoLista) {
            case 1: //Caso selecione a opção 1, organizar de forma crescente
                preverMMS(fileInfo, outputFileInfo, qntObserv, resolucao, modoInterativo, (int) nAlfa, data.split("-"));
                break;
            case 2: //Caso selecione a opção 2, organizar de forma decrescente
                preverMMEP(fileInfo, outputFileInfo, qntObserv, resolucao, modoInterativo, nAlfa, data.split("-"));
                break;
        }

        if (modoInterativo == true) {
            confirmarContinuar();
        }
    }

    public static void preverMMS(String[][] fileInfo, String[] outputFileInfo, int qntObserv, int resolucao, boolean modoInterativo, int n, String[] escolha) throws IOException {
        int iteradorData = 0; //Iterador que guarda a posição da data no array
        int firstInterval = -1, secondInterval = -1; //Guarda o inicio e o fim dos intervalos para os periodos do dia
        int day = -1, month = -1, year = -1; //Guarda a data, caso não sejam usados, são definidos para -1
        String ifFuture = ""; //Usado para diferenciar a mensagem de saída de uma previsão existente ou futura 

        if (modoInterativo == true) {
            n = definirOrdem(qntObserv);
            System.out.println("Defina a data a ser prevista, segundo a formatação.");
        }

        switch (resolucao) {
            case 4:
                if (modoInterativo == true) {
                    System.out.println("Ano");
                    ler.nextLine();
                    do {
                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 1) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 1);
                }
                year = Integer.parseInt(escolha[0]);
                break;
            case 3:
                if (modoInterativo == true) {
                    System.out.println("Mês/Ano");
                    ler.nextLine();
                    do {
                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 2) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 2);
                }
                year = Integer.parseInt(escolha[1]);
                month = Integer.parseInt(escolha[0]);
                break;
            case 2:
                if (modoInterativo == true) {
                    System.out.println("Dia/Mês/Ano");
                    ler.nextLine();
                    do {
                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 3) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 3);
                }
                year = Integer.parseInt(escolha[2]);
                month = Integer.parseInt(escolha[1]);
                day = Integer.parseInt(escolha[0]);
                break;
            case 1:
                if (modoInterativo == true) {
                    System.out.println("Periodo do Dia/Dia/Mês/Ano");
                    ler.nextLine();
                }
                do {
                    if (modoInterativo == true) {
                        do {
                            escolha = ler.nextLine().split("/");
                            if (escolha.length != 4) {
                                System.out.println("Data inválida, tente novamente.");
                            }
                        } while (escolha.length != 4);
                    }
                    year = Integer.parseInt(escolha[3]);
                    month = Integer.parseInt(escolha[2]);
                    day = Integer.parseInt(escolha[1]);
                    if (escolha[0].compareTo("Manhã") == 0) {
                        firstInterval = 6;
                        secondInterval = 12;
                    } else if (escolha[0].compareTo("Tarde") == 0) {
                        firstInterval = 12;
                        secondInterval = 18;
                    } else if (escolha[0].compareTo("Noite") == 0) {
                        firstInterval = 18;
                        secondInterval = 24;
                    } else if (escolha[0].compareTo("Madrugada") == 0) {
                        firstInterval = 0;
                        secondInterval = 6;
                    } else {
                        System.out.println("Data inválida, tente novamente.");
                    }
                } while (firstInterval == -1);
                break;
            default:
                break;
        }

        while (iteradorData < (qntObserv - 2) && !(((Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) >= firstInterval && Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) < secondInterval) || (firstInterval + secondInterval == -2)) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_DAYS]) == day || day == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_MONTHS]) == month || month == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_YEARS]) == year || year == -1))) {
            iteradorData++;
        }
        if (iteradorData == (qntObserv - 2) && !(((Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) >= firstInterval && Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) < secondInterval) || (firstInterval + secondInterval == -2)) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_DAYS]) == day || day == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_MONTHS]) == month || month == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_YEARS]) == year || year == -1))) {
            iteradorData = qntObserv - 1;
            ifFuture = " na primeira observação futura";
        }

        if (modoInterativo == true) {
            System.out.println("Consumo previsto" + ifFuture + ": " + calcularPrevisaoMMS(fileInfo, qntObserv, iteradorData, n) + " Watts");
        } else {
            outputFileInfo[1] = "info";
            String fileName = (outputFileInfo[0] + "_" + outputFileInfo[1] + "_" + outputFileInfo[2]);

            FileWriter file = new FileWriter(fileName + ".txt", true);
            BufferedWriter buffWriter = new BufferedWriter(file);
            buffWriter.write("Consumo previsto" + ifFuture + ": " + calcularPrevisaoMMS(fileInfo, qntObserv, iteradorData, n) + " Watts");
            buffWriter.newLine();
            buffWriter.close();
        }
    }

    public static int calcularPrevisaoMMS(String[][] fileInfoPreverMMS, int qntObserv, int iteradorData, int n) {

        int previsaoSoma = 0;
        for (int k = iteradorData - n; k < iteradorData; k++) {
            previsaoSoma += Integer.parseInt(fileInfoPreverMMS[k][FILEINFO_WATTS]);
        }

        return (previsaoSoma / n);
    }

    public static void preverMMEP(String[][] fileInfo, String[] outputFileInfo, int qntObserv, int resolucao, boolean modoInterativo, float alfa, String[] escolha) throws IOException {
        int iteradorData = 0; //Iterador que guarda a posição da data no array
        int firstInterval = -1, secondInterval = -1; //Guarda o inicio e o fim dos intervalos para os periodos do dia
        int day = -1, month = -1, year = -1; //Guarda a data, caso não sejam usados, são definidos para -1
        String ifFuture = ""; //Usado para diferenciar a mensagem de saída de uma previsão existente ou futura 

        if (modoInterativo == true) {
            alfa = definirAlfa();
            System.out.println("Defina a data a ser prevista, segundo a formatação.");
        }

        switch (resolucao) {
            case 4:
                if (modoInterativo == true) {
                    System.out.println("Ano");
                    ler.nextLine();
                    do {
                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 1) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 1);
                }
                year = Integer.parseInt(escolha[0]);
                break;
            case 3:
                if (modoInterativo == true) {
                    System.out.println("Mês/Ano");
                    ler.nextLine();
                    do {

                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 2) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 2);
                }
                year = Integer.parseInt(escolha[1]);
                month = Integer.parseInt(escolha[0]);
                break;
            case 2:
                if (modoInterativo == true) {
                    System.out.println("Dia/Mês/Ano");
                    ler.nextLine();
                    do {

                        escolha = ler.nextLine().split("/");
                        if (escolha.length != 3) {
                            System.out.println("Data inválida, tente novamente.");
                        }
                    } while (escolha.length != 3);
                }
                year = Integer.parseInt(escolha[2]);
                month = Integer.parseInt(escolha[1]);
                day = Integer.parseInt(escolha[0]);
                break;
            case 1:
                if (modoInterativo == true) {
                    System.out.println("Periodo do Dia/Dia/Mês/Ano");
                    ler.nextLine();
                }
                do {
                    if (modoInterativo == true) {
                        do {
                            escolha = ler.nextLine().split("/");
                            if (escolha.length != 4) {
                                System.out.println("Data inválida, tente novamente.");
                            }
                        } while (escolha.length != 4);
                    }
                    year = Integer.parseInt(escolha[3]);
                    month = Integer.parseInt(escolha[2]);
                    day = Integer.parseInt(escolha[1]);
                    if (escolha[0].compareTo("Manhã") == 0) {
                        firstInterval = 6;
                        secondInterval = 12;
                    } else if (escolha[0].compareTo("Tarde") == 0) {
                        firstInterval = 12;
                        secondInterval = 18;
                    } else if (escolha[0].compareTo("Noite") == 0) {
                        firstInterval = 18;
                        secondInterval = 24;
                    } else if (escolha[0].compareTo("Madrugada") == 0) {
                        firstInterval = 0;
                        secondInterval = 6;
                    } else {
                        System.out.println("Data inválida, tente novamente.");
                    }
                } while (firstInterval == -1);
                break;
            default:
                break;
        }

        while (iteradorData < (qntObserv - 2) && !(((Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) >= firstInterval && Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) < secondInterval) || (firstInterval + secondInterval == -2)) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_DAYS]) == day || day == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_MONTHS]) == month || month == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_YEARS]) == year || year == -1))) {
            iteradorData++;
        }
        if (iteradorData == (qntObserv - 2) && !(((Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) >= firstInterval && Integer.parseInt(fileInfo[iteradorData][FILEINFO_HOURS]) < secondInterval) || (firstInterval + secondInterval == -2)) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_DAYS]) == day || day == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_MONTHS]) == month || month == -1) && (Integer.parseInt(fileInfo[iteradorData][FILEINFO_YEARS]) == year || year == -1))) {
            iteradorData = qntObserv - 1;
            ifFuture = " na primeira observação futura";
        }
        if (modoInterativo == true) {
            System.out.println("Consumo previsto" + ifFuture + ": " + calcularPrevisaoMMEP(fileInfo, iteradorData, alfa) + " Watts");
        } else {
            outputFileInfo[1] = "info";
            String fileName = (outputFileInfo[0] + "_" + outputFileInfo[1] + "_" + outputFileInfo[2]);

            FileWriter file = new FileWriter(fileName + ".txt", true);
            BufferedWriter buffWriter = new BufferedWriter(file);
            buffWriter.write("Consumo previsto" + ifFuture + ": " + calcularPrevisaoMMEP(fileInfo, iteradorData, alfa) + " Watts");
            buffWriter.newLine();
            buffWriter.close();
        }
    }

    public static int calcularPrevisaoMMEP(String[][] fileInfoPreverMMEP, int iteradorData, float alfa) {
        if (iteradorData <= 0){
            iteradorData = 1;
        }
        
        int previsao = (int) (alfa * Integer.parseInt(fileInfoPreverMMEP[iteradorData][FILEINFO_WATTS]) + (1 - alfa) * Integer.parseInt(fileInfoPreverMMEP[iteradorData - 1][FILEINFO_WATTS]));

        return (previsao);
    }

    public static void confirmarContinuar() {
        System.out.println("Enter para continuar.");
        ler.nextLine();
    }

    public static String getFileName(String[] outputFileInfo, String extension) {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        outputFileInfo[2] = date;
        int fileNameAppend = 1;

        String fileName = (outputFileInfo[0] + "_" + outputFileInfo[1] + "_" + outputFileInfo[2]);
        while (new File(fileName + extension).isFile() == true) {//Verifica se o ficheiro já existe, e caso exista, muda o nome
            fileName = (fileName + "(" + fileNameAppend + ")");
            fileNameAppend++;
        }

        return (fileName);
    }

    public static void gnuPlot(String[] outputFileInfo, int[] arrayToPlot, int qntObserv, boolean modoInterativo) throws IOException {
        String escolha = "";
        int[][] arr = new int[qntObserv][1];
        for (int i = 0; i < qntObserv; i++) {
            arr[i][0] = arrayToPlot[i];
        }

        DataSetPlot s = new DataSetPlot(arr);

        PlotStyle myPlotStyle = new PlotStyle();
        myPlotStyle.setStyle(Style.LINES);
        myPlotStyle.setLineWidth(1);
        myPlotStyle.setLineType(NamedPlotColor.BLUE);
        myPlotStyle.setPointType(1);
        myPlotStyle.setPointSize(1);

        s.setPlotStyle(myPlotStyle);
        s.setTitle("Watts");

        if (modoInterativo == true) {
            ler.nextLine();
            System.out.println("Deseja guardar o gráfico como ficheiro .png?");
            System.out.println("Y/N");
            escolha = ler.nextLine();
        }
        if (escolha.compareTo("Y") == 0 || modoInterativo == false) {
            String fileName = getFileName(outputFileInfo, ".png");

            GNUPlotTerminal png = new FileTerminal("png", fileName + ".png");

            JavaPlot saveImage = new JavaPlot();
            saveImage.setTerminal(png);

            saveImage.addPlot(s);
            saveImage.plot();
        }

        if (modoInterativo == true) {
            System.out.println("Deseja guardar a série como ficheiro .csv?");
            System.out.println("Y/N");
            escolha = ler.nextLine();
        }
        if (escolha.compareTo("Y") == 0 || modoInterativo == false) { //Se o utilizador quiser guardar o ficheiro .csv
            String fileName = getFileName(outputFileInfo, ".csv");

            FileWriter file = new FileWriter(fileName + ".csv");
            BufferedWriter buffWriter = new BufferedWriter(file);
            for (int i = 1; i < qntObserv; i++) {
                buffWriter.write(Integer.toString(arrayToPlot[i]));
                buffWriter.newLine();
            }
            buffWriter.close();
        }

        if (modoInterativo == true) {
            JavaPlot p = new JavaPlot();

            p.addPlot(s);
            p.plot();

            confirmarContinuar(); //Confirmar e voltar ao menu
        }
    }
}
