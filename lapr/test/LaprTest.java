
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class LaprTest {
    
    public LaprTest() {
    }

    @Test
    public void testNumObservacoesTotal() {
        System.out.println("numObservacoesTotal");
        String[][] fileInfo = new String[6][7];
        fileInfo[0][6] = "1";
        fileInfo[1][6] = "2";
        fileInfo[2][6] = "3";
        fileInfo[3][6] = "4";
        fileInfo[4][6] = null;
        fileInfo[5][6] = null;
        
        int expResult = 4;
        int result = Lapr.numObservacoesTotal(fileInfo);
        assertEquals(expResult, result);
    }

    @Test
    public void testMMS() throws Exception {
        System.out.println("MMS");
        String[][] fileInfoRaw = new String[3][7];
        fileInfoRaw[0][6] = "1";
        fileInfoRaw[1][6] = "2";
        fileInfoRaw[2][6] = "3";
        
        int numObserv = 3;
        int n = 1;
        int[] expResult = {1,2,3};
        int[] result = Lapr.MMS(fileInfoRaw, numObserv, n);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testMMEP() throws Exception {
        System.out.println("MMEP");
        String[][] fileInfo = new String[3][7];
        fileInfo[0][6] = "1";
        fileInfo[1][6] = "2";
        fileInfo[2][6] = "3";
        
        int numObserv = 3;
        float alfa = 0.5F;
        int[] expResult = {1,1,1};
        int[] result = Lapr.MMEP(fileInfo, numObserv, alfa);
        assertArrayEquals(expResult, result);
    }


    @Test
    public void testCalcularPrevisaoMMS() {
        System.out.println("calcularPrevisaoMMS");
        String[][] fileInfo = new String[3][7];
        fileInfo[0][6] = "1";
        fileInfo[1][6] = "2";
        fileInfo[2][6] = "3";
        
        int qntObserv = 3;
        int iteradorData = 2;
        int n = 1;
        int expResult = 2;
        int result = Lapr.calcularPrevisaoMMS(fileInfo, qntObserv, iteradorData, n);
        assertEquals(expResult, result);
    }

    @Test
    public void testCalcularPrevisaoMMEP() {
        System.out.println("calcularPrevisaoMMEP");
        String[][] fileInfo = new String[3][7];
        fileInfo[0][6] = "1";
        fileInfo[1][6] = "2";
        fileInfo[2][6] = "3";
        
        int iteradorData = 2;
        float alfa = 0.5F;
        int expResult = 2;
        int result = Lapr.calcularPrevisaoMMEP(fileInfo, iteradorData, alfa);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFileName() {
        System.out.println("getFileName");
        String[] outputFileInfo = {"ABC","DEF","GHI"};
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        outputFileInfo[2] = date;
        String extension = ".txt";
        String expResult = ("ABC_DEF_" + date);
        String result = Lapr.getFileName(outputFileInfo, extension);
        assertEquals(expResult, result);
    }
}
